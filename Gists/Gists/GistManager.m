//
//  GistManager.m
//  Gists
//
//  Created by Felipe Bassi on 6/20/16.
//  Copyright © 2016 Felipe Bassi. All rights reserved.
//

#import "GistManager.h"
#import "ConnectionManager.h"

@interface GistManager ()

@property(strong, nonatomic, readonly) ConnectionManager *objectManager;

@end

@implementation GistManager

-(void)getGistListWithParameters:(NSDictionary *)parameters withCompletionHandler:(resultBlock)handler{    
    [[ConnectionManager new] getObjectsAtPath:@"public" withParameters:parameters withCompletionHandler:^(id responseObject, NSError *error) {
        if (!error) {
            NSLog(@"%@",responseObject);
            
            
            
//            GistBase *gist = [[GistBase alloc]initWithDictionary:responseObject];
//            Invites *invitesList = [[Invites alloc]initWithDictionary:responseResult];
//            handler(invitesList,error);
        }else{
            handler(nil,error);
        }
    }];
}

@end
