//
//  ViewController.m
//  Gists
//
//  Created by Felipe Bassi on 6/20/16.
//  Copyright © 2016 Felipe Bassi. All rights reserved.
//

#import "ViewController.h"
#import "GistCollectionViewCell.h"
#import "GistManager.h"

#define GIST_WIDTH 151
#define GIST_HEIGHT 104
#define GIST_SIZE (CGSize) CGSizeMake(GIST_WIDTH, GIST_HEIGHT)

@interface ViewController () <UICollectionViewDelegate, UICollectionViewDataSource>{
    CGSize gistSize;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionGists;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Calculate the size collectionViewCell to always have two of them presented on the screen
    CGFloat screenWidth = CGRectGetWidth(self.view.bounds);
    CGFloat twoPiecesWidth = floor((screenWidth / 2.0) - 16); // 16 are the margim factor 8 on right and 8 on left
    CGFloat prop = (100*twoPiecesWidth/GIST_WIDTH)/100; // calculating the proportion of height on a gist cell
    CGFloat gistHeight = floor(GIST_HEIGHT*prop);
    gistSize = CGSizeMake(twoPiecesWidth, gistHeight);
    
    self.title = @"Gists";
    
    [self getGistList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 20;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cellGist";
    
    GistCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.imgGistOwner.image = [UIImage imageNamed:@"anonymous"];
    cell.lblGistName.text = @"anonymous";
    cell.lblGistType.text = @"text/plain";
    cell.lblGistLanguage.text = @"Markdown";
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return gistSize;
}

#pragma mark - Private Methods

-(void)getGistList{
    [[GistManager new] getGistListWithParameters:@{@"page":@"0"} withCompletionHandler:^(id responseObject, NSError *error) {
        
    }];
}
@end
