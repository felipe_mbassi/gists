//
//  ConnectionManager.m
//  Gists
//
//  Created by Felipe Bassi on 6/20/16.
//  Copyright © 2016 Felipe Bassi. All rights reserved.
//

#import "ConnectionManager.h"
#import <AFNetworking.h>

const NSString *kServiceUrl = @"https://api.github.com/gists/";

@implementation ConnectionManager

-(void)getObjectsAtPath:(NSString *)path withParameters:(NSDictionary *)parameters withCompletionHandler:(resultBlock)handler {
    NSString *urlPath = [kServiceUrl stringByAppendingString:path];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:urlPath parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        handler(responseObject, nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        handler(nil, error);
    }];
}


@end
