//
//  GistManager.h
//  Gists
//
//  Created by Felipe Bassi on 6/20/16.
//  Copyright © 2016 Felipe Bassi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^resultBlock)(id responseObject, NSError *error);

@interface GistManager : NSObject

-(void)getGistListWithParameters:(NSDictionary *)parameters withCompletionHandler:(resultBlock)handler;

@end
