//
//  GistCollectionViewCell.h
//  Gists
//
//  Created by Felipe Bassi on 6/20/16.
//  Copyright © 2016 Felipe Bassi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GistCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgGistOwner;
@property (weak, nonatomic) IBOutlet UILabel *lblGistName;
@property (weak, nonatomic) IBOutlet UILabel *lblGistType;
@property (weak, nonatomic) IBOutlet UILabel *lblGistLanguage;

@end
